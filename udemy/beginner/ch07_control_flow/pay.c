#include <stdio.h>

const int MAX_HOURS_WEEK = 40;
const int PAY_RATE = 12;

const float INIT_TAX_RATE = 0.15;
const float MIDDLE_TAX_RATE = 0.2;
const float FINAL_TAX_RATE = 0.25;

int main()
{
    int hours, overtime = 0;
    float groosPay, netPay = 0.0;
    float firstTax, secondTax, finalTax = 0.0;

    printf("Enter hours worked this week to calculate gross pay, net pay and taxes: ");
    scanf("%d", &hours);

    if (hours > 0) {
        if (hours > MAX_HOURS_WEEK) {
            overtime = hours - MAX_HOURS_WEEK;
            hours = MAX_HOURS_WEEK;
        }

        groosPay = hours * PAY_RATE;
        if (overtime > 0) {
            groosPay += overtime * (PAY_RATE * 1.5);
        }

        if (groosPay > 450) {
            firstTax = 300 * INIT_TAX_RATE;
            secondTax = 150 * MIDDLE_TAX_RATE;
            finalTax = (groosPay - 450) * FINAL_TAX_RATE;
        } else if (groosPay > 300) {
            firstTax = 300 * INIT_TAX_RATE;
            secondTax = (groosPay - 300) * MIDDLE_TAX_RATE;
        } else {
            firstTax = groosPay * INIT_TAX_RATE;
        }

        netPay = groosPay - (firstTax + secondTax + finalTax);

        printf("Gross Pay:\t\t\t$ %4.2f\n", groosPay);
        printf("-15%% of the first $ 300.00:\t$ %4.2f\n", firstTax);
        printf("-20%% of the next  $ 150.00:\t$ %4.2f\n", secondTax);
        printf("-25%% of the rest:\t\t$ %4.2f\n", finalTax);
        printf("Net Pay:\t\t\t$ %4.2f\n", netPay);
    } else {
        printf("Unable to calculate payment and taxes.\n");
    }

    return 0;
}