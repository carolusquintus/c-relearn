#include <stdio.h>

enum Day { MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY };

int main()
{
    for (enum Day day = MONDAY; day <= SUNDAY; ++day) {
        if (day == WEDNESDAY)
            continue;

        printf("Itś not Wednesday!\n");
    }
    return 0;
}