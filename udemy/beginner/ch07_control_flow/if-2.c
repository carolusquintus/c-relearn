#include <stdio.h>

int main()
{
    int number, remainder;

    printf("Enter a number to test if is even or odd:\n");
    scanf("%i", &number);

    remainder = number % 2;

    if (remainder == 0)
        printf("Number %d is even\n", number);
    else
        printf("Number %d is odd\n", number);

    return 0;
}
