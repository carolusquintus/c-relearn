#include <stdio.h>

int main()
{
    float v1, v2;
    char operator;

    printf("Type in your expression:\n");
    scanf("%f %c %f", &v1, &operator, &v2);

    switch (operator) {
        case '+':
            printf("%.2f\n", v1 + v2);
        break;
        case '-':
            printf("%.2f\n", v1 - v2);
        break;
        case '*':
            printf("%.2f\n", v1 * v2);
        break;
        case '/':
            if (v2 == 0)
                printf("Divison by zero\n");
            else
                printf("%.2f\n", v1 / v2);
        break;
        default:
            printf("Unknown operator\n");
        break;
    }
    return 0;
}