#include <stdio.h>

int main()
{
    int number;

    printf("Enter a number to test if is negative, positive or zero:\n");
    scanf("%i", &number);

    if (number < 0)
        printf("%d is negative\n", number);
    else if (number == 0)
        printf("%d is zero\n", number);
    else
        printf("%d is positive\n", number);

    return 0;
}
