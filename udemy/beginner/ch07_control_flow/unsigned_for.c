#include <stdio.h>

int main()
{
    unsigned long long sum = 0;
    unsigned int count = 0;

    printf("Enter the number of integers you want to sum: ");
    scanf("%u", &count);

    for (unsigned int i = 0; i <= count; sum += i++)
        printf("i = %i, sum = %llu\n", i, sum);

    printf("Total of the first %u numbers is %llu\n", count, sum);

    return 0;
}