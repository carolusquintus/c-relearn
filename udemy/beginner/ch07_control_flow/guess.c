#include <time.h>
#include <stdio.h>
#include <stdlib.h>

const int MAX_TRIES = 5;

int main()
{
    time_t t;
    srand((unsigned) time(&t));

    int randomNumber = rand() % 21;

    unsigned int guess = 0;

    printf("Guess the number!!!\n\n");
    printf("This is a guessing game.\n");
    printf("I have chosen a number between 0 and 20 which you must guess.\n\n");

    for (int try = MAX_TRIES; try >= 0; --try) {
        if (try == 0) {
            printf("You failed!!! The number was %d\n", randomNumber);
            break;
        }

        printf("You have %d tries left\n", try);
        printf("Enter a guess: ");
        scanf("%ui", &guess);

        if (guess < 0 || guess > 20) {
            printf("I'm completely sure I said between 0 and 20. :(\n\n");
            continue;
        }

        if (randomNumber < guess) {
            printf("Sorry, %d is wrong. My number is less than that.\n\n", guess);
            continue;
        }

        if (randomNumber > guess) {
            printf("Sorry, %d is wrong. My number is greater than that.\n\n", guess);
            continue;
        }

        printf("Congratulations!!! You guessed it!!!\n\n");
        break;
    }

    return 0;
}