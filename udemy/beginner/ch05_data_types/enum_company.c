#include <stdio.h>

enum COMPANY {
    GOOGLE, FACEBOOK, XEROX, YAHOO, EBAY, MICROSOFT
};

int main()
{
    enum COMPANY xerox = XEROX;
    enum COMPANY google = GOOGLE;
    enum COMPANY ebay = EBAY;

    printf("XEROX is: %d\n", xerox);
    printf("GOOGLE is: %d\n", google);
    printf("EBAY is: %d\n", ebay);

    return 0;
}