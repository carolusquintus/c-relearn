#include <stdio.h>
#include <stdbool.h>

int main()
{
    int intVar = 100;
    float floatVar = 331.79;
    double doubleVar = 8.44e+11;
    char charVar = 'W';
    bool boolVar = 0;

    printf("intVar = %i\n", intVar);
    printf("floatVar = %f\n", floatVar);
    printf("doubleVar = %e\n", doubleVar);
    printf("doubleVar = %g\n", doubleVar);
    printf("charVar = %c\n", charVar);
    printf("boolVar = %i\n", boolVar);

    printf("50%%\n");

    float x = 3.99932323232;
    printf("%.3f\n", x);

    return 0;
}