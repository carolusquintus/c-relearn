#include <stdio.h>

int main()
{
    double width = 40.8, height = 90.7;
    double perimeter = 2.0 * (height + height);
    double area = width * height;

    printf("Width is: %g\nHeight is: %g\nPerimeter is: %g\nArea is: %g\n", width, height, perimeter, area);

    return 0;
}