#include <stdio.h>
#include <stdbool.h>

enum primaryColor { red = 0xFF0000, green = 0x00FF00, blue = 0x0000FF};

enum gender { male, female };

int main()
{
    enum primaryColor dot1 = red;
    enum primaryColor dot2 = green;
    enum primaryColor dot3 = blue;

    printf("%d\n", dot1);
    printf("%d\n", dot2);
    printf("%d\n", dot3);

    enum gender myGender = male;
    enum gender newGender = female;

    bool isMale = (myGender == newGender);

    printf("%d\n", isMale);

    return 0;
}
