#include <stdio.h>

int main()
{
    double minutes, days, years = 0.0;

    printf("Enter quantity of minutes to calculate:\n");
    scanf("%lf", &minutes);

    days = minutes / 60 / 24;
    years = days / 365;

    printf("Minutes: %g\n", minutes);
    printf("Days: %g\n", days);
    printf("Years: %g\n", years);
    return 0;
}