#include <stdio.h>

int main(int argc, char *argv[])
{
    int numberOfArgs = argc;
    char *arg0 = argv[0];
    char *arg1 = argv[1];

    printf("Number of arguments: %d\n", numberOfArgs);
    printf("Argument 0 is the program name: %s\n", arg0);
    printf("Argument 1 is the command line argument: %s\n", arg1);

    return 0;
}