#include <stdio.h>

#define MIN 3
#define MAX 100000

int main()
{
    int primes[10000] = { 2 };
    int totalPrimes = 1;

    for (int number = MIN; number <= MAX; ++number)
        if (number % 2 == 0)
            continue;
        else
            for (int i = 0; i < totalPrimes; ++i)
                if (number % primes[i] == 0)
                    break;
                else if (i == totalPrimes - 1) {
                    primes[totalPrimes++] = number;
                    break;
                }
                else
                    continue;

    for (int j = 0; j < totalPrimes; ++j)
        printf("%d, ", primes[j]);
    printf("\n");

    return 0;
}
