#include <stdio.h>

int main()
{
    int grades[10];
    int count = 10;

    long sum = 0;
    float average = 0.0f;

    printf("\nEnter the 10 grades:\n");

    for (int i = 0; i < count; ++i) {
        printf("%2u> ", i + 1);
        scanf("%d", &grades[i]);
        sum += grades[i];
    }

    average = (float) sum / count;

    printf("\nAverage of the ten grades entered is %.2f\n", average);

    float sample[500] = {[2] = 500.5, [1] = 300.0, [4] = 100.1};

    for (int j = 0; j < 20; j++)
        printf("%.2f\n", sample[j]);

    return 0;
}