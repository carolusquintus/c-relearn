#include <stdio.h>

int main()
{
    int numbers[3][4] = {
        { 10, 20, 30, 40 },
        { 15, 25, 35, 45 },
        { 47, 48, 49, 50 },
    };
    // Both are enabled
    // int numbers[3][4] = {
    //     10, 20, 30, 40,
    //     15, 25, 35, 45,
    //     47, 48, 49, 50,
    // };

    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 4; ++j)
            printf("%d,", numbers[i][j]);
        printf("\n");
    }

    printf("\n");

    int matrix[4][5] = {
        { 10,  5, -3 },
        {  9, -8,  3 },
        { 32, 20,  1 },
        {  6, -7,  8 },
    };

    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 5; ++j)
            printf("%d,", matrix[i][j]);
        printf("\n");
    }

    printf("\n");

    int matrix_identity[3][3] = { [0][0] = 1, [1][1] = 1, [2][2] = 1 };

    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
            printf("%d,", matrix_identity[i][j]);
        printf("\n");
    }
    return 0;
}