#include <stdio.h>

int main()
{
    int numbers[2][3][4] = {
        {
            { 10, 20, 30, 40 },
            { 15, 25, 35, 45 },
            { 47, 48, 49, 50 },
        },
        {
            { 10, 20, 30, 40 },
            { 15, 25, 35, 45 },
            { 47, 48, 49, 50 },
        },
    };

    for (int i = 0; i < 2; ++i){
        for (int j = 0; j < 3; ++j) {
            for (int k = 0; k < 4; ++k) {
                printf("[%d][%d][%d] = %d\n", i, j, k, numbers[i][j][k]);
            }
            printf("\n");
        }
        printf("\n");
    }

    printf("\n");

    int three[3][3][3] = { [0][0][0] = 1, [1][1][1] = 1, [2][2][2] = 1 };

    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            for (int k = 0; k < 3; ++k)
            {
                printf("[%d][%d][%d] = %d", i, j, k, three[i][j][k]);
            }
        }
        printf("\n");
    }

    return 0;
}