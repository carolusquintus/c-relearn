#include <stdio.h>

int main()
{
    printf("int is size of %zd bytes\n", sizeof(int));
    printf("short is size of %zd bytes\n", sizeof(short));
    printf("char is size of %zd bytes\n", sizeof(char));
    printf("long is size of %zd bytes\n", sizeof(long));
    printf("long long is size of %zd bytes\n", sizeof(long long));
    printf("float is size of %zd bytes\n", sizeof(float));
    printf("double is size of %zd bytes\n", sizeof(double));
    printf("long double is size of %zd bytes\n", sizeof(long double));
    return 0;
}