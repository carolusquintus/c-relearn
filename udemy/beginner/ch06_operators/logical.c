#include <stdio.h>
#include <stdbool.h>

int main()
{
    bool a = true;
    bool b = false;
    bool result;

    result = a && b;
    printf("a && b is %i\n", result);

    result = a || b;
    printf("a || b is %i\n", result);

    result = !(a || b);
    printf("!(a || b) is %i\n", result);

    return 0;
}
