#include <stdio.h>

int main()
{
    char str[100];
    int i;

    printf("Enter your Name:\n");
    scanf("%s", str);

    printf("Enter a value:\n");
    scanf("%d", &i);

    printf("You entered: %s %d\n", str, i);

    return 0;
}
