#include<stdio.h>

#define YEARS 5
#define MONTHS 12

int main()
{
    float rain[YEARS][MONTHS] = {
        { 7.8, 5.7, 3.1, 0.2, 8.2, 9.1, 1.3, 6.6, 6.0, 7.0, 0.7, 7.5 },
        { 9.4, 9.5, 5.2, 3.2, 0.7, 7.1, 4.8, 4.8, 1.4, 0.8, 3.4, 1.7 },
        { 0.8, 9.3, 7.0, 0.0, 0.1, 0.5, 1.5, 4.2, 4.5, 8.8, 3.1, 2.3 },
        { 8.7, 7.8, 8.4, 2.5, 3.4, 2.2, 7.1, 8.6, 3.5, 5.1, 2.7, 3.1 },
        { 6.5, 6.9, 6.6, 0.0, 7.7, 9.3, 9.1, 7.6, 3.2, 6.0, 7.8, 7.6 },
    };

    float totalRainfall = 0;

    printf("YEAR\tRAINFALL (inches)\n");
    for (int year = 0; year < YEARS; ++year)
    {
        float yearlyRainfall = 0;
        for (int month = 0; month < MONTHS; ++month)
            yearlyRainfall += rain[year][month];

        printf("%d\t%.2f\n", (2010 + year), yearlyRainfall);
        totalRainfall += yearlyRainfall;

    }
    printf("\nThe yearly average is %.2f inches.\n", totalRainfall / YEARS);

    printf("\nMONTHLY AVERAGES:\n");
    printf("Jan\t\tFeb\t\tMar\t\tApr\t\tMay\t\tJun\t\tJul\t\tAug\t\tSep\t\tOct\t\tNov\t\tDec\n");
    for (int month = 0; month < MONTHS; ++month)
    {
        float totalMonthlyRainfall = 0;
        for (int year = 0; year < YEARS; ++year)
        {
            totalMonthlyRainfall += rain[year][month];
        }
        printf("%.2f\t", totalMonthlyRainfall / YEARS);
    }
    printf("\n");
    return 0;
}